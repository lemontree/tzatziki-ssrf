package io.tzatziki.database;

import io.tzatziki.database.TestCaseEntity;
import io.tzatziki.database.TestCaseManager;

public class TestCaseManagerTest {
    public static void main(String[] args) {
        TestCaseManager manager = new TestCaseManager();
        TestCaseEntity testCase = manager.getTestCase(10946, "Second test case");
        System.out.println("testCase = " + testCase);

        testCase.getStepState().put(1,"PASSED");
        testCase.getStepState().put(2,"FAILED");
        testCase.getStepState().put(8,"SKIPPED");
        testCase.getStorage().put("kalle.class","fast_nä");

        manager.persist();
    }
}

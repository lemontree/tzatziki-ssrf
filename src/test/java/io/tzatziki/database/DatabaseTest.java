package io.tzatziki.database;

import io.tzatziki.properties.TzatzikiProperties;
import io.tzatziki.database.TestCaseEntity;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.hibernate.query.criteria.HibernateCriteriaBuilder;

import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

public class DatabaseTest {
    public static void main(String[] args) {
        TzatzikiProperties properties = new TzatzikiProperties();

        // Turn off some annoying logging
        java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);

        // Properties should be set up once, and is used once for creating the thread safe SessionFactory.
        // The session factory should be kept in the TzatzikiCache object. One session object per database.

        // Using Hibernate, set up everything using dynamic properties
        Properties prop = new Properties();
        prop.setProperty("hibernate.connection.url", properties.getString("tzatziki.db.url"));
        prop.setProperty("hibernate.connection.driver_class", properties.getString("tzatziki.db.driver"));
        prop.setProperty("hibernate.connection.username", properties.getString("tzatziki.db.username"));
        prop.setProperty("hibernate.connection.password", properties.getString("tzatziki.db.password"));
        prop.setProperty("hibernate.show_sql", properties.getString("tzatziki.db.show_sql"));
        prop.setProperty("hibernate.hbm2ddl.auto", "create"); // Recreate database on every run, for debugging.

        // Create a factory, and add in all entities we want to handle
        SessionFactory factory = new Configuration()
                .addProperties(prop)
                .addAnnotatedClass(TestCaseEntity.class)
                .buildSessionFactory();

        // Sessions are lightweight, and should be created anew for each time we want to interact with the
        // database objects. Not thread safe, should be thrown away after use. Transaction use is optional
        // but can optimize things as per usual. Queries are used to get data from base.
        // When saving data, we can use maps of things, lists, arrays etc, as long as it contains other
        // entities!

        // Begin a transaction
        Session session = factory.openSession();
        try {
            session.beginTransaction();

            // Create new data and persist it to storage
            TestCaseEntity newTestCaseEntity = new TestCaseEntity(4711,"First test case");
            newTestCaseEntity.getStepState().put(1,"PASSED");
            newTestCaseEntity.getStepState().put(3,"FAILED");
            newTestCaseEntity.getStepState().put(2,"PASSED");
            newTestCaseEntity.getStepState().put(8,"UNKNOWN");
            session.persist(newTestCaseEntity);
            System.out.println("state = " + newTestCaseEntity);
            Long id = newTestCaseEntity.getId();

            // Create another piece of data and persist it to storage
            TestCaseEntity newerTestCaseEntity = new TestCaseEntity(17, "Second test case");
            newerTestCaseEntity.getStepState().put(1,"PASSED");
            newerTestCaseEntity.getStepState().put(2,"PASSED");
            session.persist(newerTestCaseEntity);
            System.out.println("state = " + newerTestCaseEntity);

            // Get all states and manipulate them if needed
            Query<TestCaseEntity> query = session.createQuery("FROM TestCaseEntity", TestCaseEntity.class);
            List<TestCaseEntity> results = query.list();
            System.out.println("All test cases:");
            for (TestCaseEntity testCaseEntity : results) {
                System.out.println(testCaseEntity);
            }

            // Build a query by HQL
            Query<TestCaseEntity> queryFirst = session.createQuery("FROM TestCaseEntity T WHERE T.name = :name", TestCaseEntity.class);
            queryFirst.setParameter("name","First test case");
            results = queryFirst.list();
            System.out.println("First test case(s):");
            for (TestCaseEntity testCaseEntity : results) {
                System.out.println(testCaseEntity);
            }

            // Build a query by QueryBuilder using Criteria
            HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<TestCaseEntity> cr = cb.createQuery(TestCaseEntity.class);
            Root<TestCaseEntity> r = cr.from(TestCaseEntity.class);
            CriteriaQuery<TestCaseEntity> cq = cr.select(r).where(cb.equal(r.get("name"), "Second test case"));
            Query<TestCaseEntity> q = session.createQuery(cq);
            results = q.list();
            System.out.println("Second test case(s) by criteria:");
            for (TestCaseEntity testCaseEntity : results) {
                System.out.println(testCaseEntity);
            }

            // We then get the first one we created and remove it from the database
            TestCaseEntity testCaseEntity = session.get(TestCaseEntity.class, id);
            session.remove(testCaseEntity);

            // Finally commit the transaction and all will be saved
            session.getTransaction().commit();
        }
        catch (Exception e) {
            if (session.getTransaction()!=null) session.getTransaction().rollback();
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        factory.close();
    }
}

package io.tzatziki.utility;

import org.joda.time.DateTime;

import static io.tzatziki.utility.AnsiColor.CYAN;
import static io.tzatziki.utility.AnsiColor.RESET;

/**
 * Stack utility.
 */
public class StackUtil {

    /**
     * Log function.
     *
     * @param output set to true to enable output.
     */
    public static void log_function(boolean output) {
        if (output) {
            StackTraceElement ste = new Throwable().getStackTrace()[1];
            if (ste.getClassName().contains("io.tzatziki")) {
                System.out.println(CYAN + DateTime.now() + " " + ste.getClassName() + "::" + ste.getMethodName() + RESET);
            } else {
                System.out.println(DateTime.now() + " " + ste.getClassName() + "::" + ste.getMethodName());
            }
        }
    }

    /**
     * Log function.
     */
    public static void log_function() {
        StackTraceElement ste = new Throwable().getStackTrace()[1];
        if (ste.getClassName().contains("io.tzatziki")) {
            System.out.println(CYAN + DateTime.now() + " " + ste.getClassName() + "::" + ste.getMethodName() + RESET);
        } else {
            System.out.println(DateTime.now() + " " + ste.getClassName() + "::" + ste.getMethodName());
        }
    }
}

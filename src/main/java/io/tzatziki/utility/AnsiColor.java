package io.tzatziki.utility;

/**
 * Ansi output manipulation constants.
 * <p>
 * For colors see: https://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html#colors
 */
@SuppressWarnings("ALL")
public class AnsiColor {
    public final static String BLACK = "\u001b[30m";
    public final static String RED = "\u001b[31m";
    public final static String GREEN = "\u001b[32m";
    public final static String YELLOW = "\u001b[33m";
    public final static String BLUE = "\u001b[34m";
    public final static String MAGENTA = "\u001b[35m";
    public final static String CYAN = "\u001b[36m";
    public final static String WHITE = "\u001b[37m";
    public final static String RESET = "\u001b[0m";

    public final static String BRIGHT_BLACK = "\u001b[30;1m";
    public final static String BRIGHT_RED = "\u001b[31;1m";
    public final static String BRIGHT_GREEN = "\u001b[32;1m";
    public final static String BRIGHT_YELLOW = "\u001b[33;1m";
    public final static String BRIGHT_BLUE = "\u001b[34;1m";
    public final static String BRIGHT_MAGENTA = "\u001b[35;1m";
    public final static String BRIGHT_CYAN = "\u001b[36;1m";
    public final static String BRIGHT_WHITE = "\u001b[37;1m";

    public final static String BG_BLACK = "\u001b[40m";
    public final static String BG_RED = "\u001b[41m";
    public final static String BG_GREEN = "\u001b[42m";
    public final static String BG_YELLOW = "\u001b[43m";
    public final static String BG_BLUE = "\u001b[44m";
    public final static String BG_MAGENTA = "\u001b[45m";
    public final static String BG_CYAN = "\u001b[46m";
    public final static String BG_WHITE = "\u001b[47m";

    public final static String BG_BRIGHT_BLACK = "\u001b[40;1m";
    public final static String BG_BRIGHT_RED = "\u001b[41;1m";
    public final static String BG_BRIGHT_GREEN = "\u001b[42;1m";
    public final static String BG_BRIGHT_YELLOW = "\u001b[43;1m";
    public final static String BG_BRIGHT_BLUE = "\u001b[44;1m";
    public final static String BG_BRIGHT_MAGENTA = "\u001b[45;1m";
    public final static String BG_BRIGHT_CYAN = "\u001b[46;1m";
    public final static String BG_BRIGHT_WHITE = "\u001b[47;1m";

    public final static String BOLD = "\u001b[1m";
    public final static String UNDERLINE = "\u001b[4m";
    public final static String REVERSED = "\u001b[7m";
}

package io.tzatziki;

import io.tzatziki.plugin.TzatzikiCache;

/**
 * Tzatziki storage - To be included in the step definitions by PicoContainer.
 */
public class Tzatziki {
    private final TzatzikiCache tzatzikiCache = TzatzikiCache.getInstance();

    /**
     * Skippable step - test if the current step can be skipped according to previous state.
     *
     * @return true if step can be skipped
     */
    public boolean skippableStep() {
        String storedState = tzatzikiCache.getStepState();
        return storedState.equalsIgnoreCase("PASSED");
    }

    /**
     * Register initializer for restoring previous values of a storage.
     *
     * @param storageClass the storage class
     * @param initializer  the initializer
     */
    public void registerInitializer(Class<?> storageClass, TzatzikiInitializer initializer) {
        tzatzikiCache.registerInitializer(storageClass,initializer);
    }
}

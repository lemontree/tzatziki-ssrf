package io.tzatziki.factory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.core.backend.ObjectFactory;
import io.tzatziki.plugin.TzatzikiCache;
import io.tzatziki.TzatzikiInitializer;
import io.tzatziki.properties.TzatzikiProperties;
import io.tzatziki.utility.StackUtil;

import java.lang.reflect.Constructor;
import java.util.Map;

import static io.tzatziki.utility.AnsiColor.CYAN;
import static io.tzatziki.utility.AnsiColor.RESET;

/**
 * Tzatziki object factory, delegating to PicoFactory when necessary to create new objects and
 * calling out for registered initializers to copy objects from storage when necessary.
 */
public class TzatzikiFactory implements ObjectFactory {

    private final TzatzikiPicoFactory delegate = new TzatzikiPicoFactory();
    private final TzatzikiProperties properties = new TzatzikiProperties();
    private final TzatzikiCache cache = TzatzikiCache.getInstance();
    private final boolean print_functions = properties.getBoolean("print.factory.functions");
    private final boolean print_glueclass = properties.getBoolean("print.factory.glueclass");
    private final boolean print_storage = properties.getBoolean("print.factory.storage");

    /**
     * Instantiates a new Tzatziki factory.
     */
    public TzatzikiFactory() {
        StackUtil.log_function(print_functions);
    }

    public void start() {
        StackUtil.log_function(print_functions);

        // Call delegate
        delegate.start();
    }

    public void stop() {
        StackUtil.log_function(print_functions);

        // Reset initialization
        for (Class<?> stg: cache.getInitializers().keySet()) {
            cache.setInitialized(stg,false);
        }

        // Call delegate
        delegate.stop();
    }

    public boolean addClass(Class<?> glueClass) {
        StackUtil.log_function(print_functions);

        // Print glue class with parameters
        if (print_glueclass) {
            System.out.println(CYAN + "className = " + glueClass.getName());
            Constructor<?>[] constructors = glueClass.getConstructors();
            for (Constructor<?> constructor : constructors) {
                Class<?>[] parameterTypes = constructor.getParameterTypes();
                for (Class<?> parameterType : parameterTypes) {
                    String typeName = parameterType.getName();
                    System.out.println("typeName  = " + typeName);
                }
            }
            System.out.print(RESET);
        }

        // Call delegate
        return delegate.addClass(glueClass);
    }

    public <T> T getInstance(Class<T> glueClass) {
        StackUtil.log_function(print_functions);

        // Call delegate and collect result
        T instance = delegate.getInstance(glueClass);

        try {
            Map<Class<?>, TzatzikiInitializer> initializers = cache.getInitializers();
            for (Class<?> stg : initializers.keySet()) {
                if (!cache.getInitialized(stg)) {
                    // Get live and stored objects
                    Object live = delegate.getInstance(stg);
                    Object stored = cache.getPersistedStorage(stg);

                    // If we have data in storage we call initializer
                    if (stored != null) {
                        TzatzikiInitializer tzatzikiInitializer = initializers.get(stg);
                        tzatzikiInitializer.initialize(live, stored);
                    }

                    // Either case we are now initialized and remember the live object
                    cache.setInitialized(stg,true);
                    cache.setLiveStorage(stg, live);

                    // Output initialized storage
                    if (print_storage) {
                        String init = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(live);
                        System.out.println(CYAN + stg.getName() + " = " + init + RESET);
                    }
                }
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        // Return result of delegate call
        return instance;
    }
}

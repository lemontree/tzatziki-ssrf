package io.tzatziki.factory;

import org.picocontainer.MutablePicoContainer;
import org.picocontainer.PicoBuilder;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

/**
 * This is a reimplementation of PicoFactory of cucumber-picocontainer. It is included simply to
 * remove the ambiguous use of two Object Factories for cucumber.
 */
public class TzatzikiPicoFactory {
    private final Set<Class<?>> classes = new HashSet<>();
    private MutablePicoContainer pico;

    public TzatzikiPicoFactory() {
    }

    private static boolean isInstantiable(Class<?> clazz) {
        boolean isNonStaticInnerClass = !Modifier.isStatic(clazz.getModifiers()) && clazz.getEnclosingClass() != null;
        return Modifier.isPublic(clazz.getModifiers()) && !Modifier.isAbstract(clazz.getModifiers()) && !isNonStaticInnerClass;
    }

    public void start() {
        this.pico = (new PicoBuilder()).withCaching().withLifecycle().build();
        for (Class<?> aClass : this.classes) {
            this.pico.addComponent(aClass);
        }
        this.pico.start();
    }

    public void stop() {
        this.pico.stop();
        this.pico.dispose();
    }

    @SuppressWarnings("SameReturnValue")
    public boolean addClass(Class<?> clazz) {
        if (isInstantiable(clazz) && this.classes.add(clazz)) {
            this.addConstructorDependencies(clazz);
        }
        return true;
    }

    public <T> T getInstance(Class<T> type) {
        return this.pico.getComponent(type);
    }

    private void addConstructorDependencies(Class<?> clazz) {
        for (Constructor<?> constructor : clazz.getConstructors()) {
            for (Class<?> paramClazz : constructor.getParameterTypes()) {
                this.addClass(paramClazz);
            }
        }
    }
}

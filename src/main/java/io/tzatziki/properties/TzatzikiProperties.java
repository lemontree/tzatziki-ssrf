package io.tzatziki.properties;

import java.util.Properties;

/**
 * Tzatziki properties - either default or read from tzatziki.properties file.
 */
@SuppressWarnings("unused")
public class TzatzikiProperties {

    private final Properties properties;

    /**
     * Instantiates a new Tzatziki properties.
     */
    public TzatzikiProperties() {
        // Creating defaults
        properties = new Properties();
        properties.setProperty("print.plugin.functions",  "false");
        properties.setProperty("print.plugin.testcase",   "false");
        properties.setProperty("print.plugin.teststep",   "false");
        properties.setProperty("print.factory.glueclass", "false");
        properties.setProperty("print.factory.functions", "false");
        properties.setProperty("print.factory.storage",   "false");
        properties.setProperty("print.manager.functions", "false");
        properties.setProperty("print.db.functions",      "false");
        properties.setProperty("tzatziki.db.driver",      ""     );
        properties.setProperty("tzatziki.db.url",         ""     );
        properties.setProperty("tzatziki.db.username",    ""     );
        properties.setProperty("tzatziki.db.password",    ""     );
        properties.setProperty("tzatziki.db.show_sql",    "false");
        properties.setProperty("tzatziki.file.path",      ""     );
        properties.setProperty("tzatziki.reset",          "false");

        // Try to read overrides from properties file
        try {
            properties.load(getClass()
                            .getClassLoader()
                            .getResourceAsStream("tzatziki.properties"));
        } catch (Exception ignore) { }
    }

    /**
     * Gets boolean.
     *
     * @param key the key
     * @return the boolean
     */
    public boolean getBoolean(String key) {
        String property = System.getProperty(key);
        if (property!=null) return Boolean.parseBoolean(property);
        return Boolean.parseBoolean(properties.getProperty(key));
    }

    /**
     * Gets int.
     *
     * @param key the key
     * @return the int
     */
    public int getInt(String key) {
        String property = System.getProperty(key);
        if (property!=null) return Integer.parseInt(property);
        return Integer.parseInt(properties.getProperty(key));
    }

    /**
     * Gets string.
     *
     * @param key the key
     * @return the string
     */
    public String getString(String key) {
        String property = System.getProperty(key);
        if (property!=null) return property;
        return properties.getProperty(key);
    }
}

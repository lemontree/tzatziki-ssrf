package io.tzatziki;

public interface TzatzikiInitializer {
    void initialize(Object live, Object store);
}

package io.tzatziki.database;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.tzatziki.properties.TzatzikiProperties;
import io.tzatziki.utility.StackUtil;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.hibernate.query.criteria.HibernateCriteriaBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Properties;

/**
 * TestCase state and storage database manager.
 */
public class TestCaseManager {
    private final TzatzikiProperties properties = new TzatzikiProperties();
    private SessionFactory sessionFactory = null;
    private TestCaseCollection testCaseCollection;
    private final boolean print_functions = properties.getBoolean("print.db.functions");
    private final boolean tzatziki_reset = properties.getBoolean("tzatziki.reset");

    /**
     * Instantiates a new Test case manager.
     */
    public TestCaseManager() {
        StackUtil.log_function(print_functions);

        // We fetch the storage strategy from properties.
        String stateUrl = properties.getString("tzatziki.db.url");
        String filePath = properties.getString("tzatziki.file.path");

        // By default, we create an empty collection to save things in...
        testCaseCollection = new TestCaseCollection();

        // ...then set up database access if a database is defined...
        if (!stateUrl.isEmpty()) {
            // Using Hibernate, set up everything using dynamic properties
            Properties prop = new Properties();
            prop.setProperty("hibernate.connection.url", properties.getString("tzatziki.db.url"));
            prop.setProperty("hibernate.connection.driver_class", properties.getString("tzatziki.db.driver"));
            prop.setProperty("hibernate.connection.username", properties.getString("tzatziki.db.username"));
            prop.setProperty("hibernate.connection.password", properties.getString("tzatziki.db.password"));
            prop.setProperty("hibernate.show_sql", properties.getString("tzatziki.db.show_sql"));
            prop.setProperty("hibernate.hbm2ddl.auto", "update");

            // Create a factory, and add in all entities we want to handle
            sessionFactory = new Configuration().addProperties(prop).addAnnotatedClass(TestCaseEntity.class).buildSessionFactory();
        }
        // ...or read from file if a path is defined
        else if (!filePath.isEmpty()) {
            try {
                String dataString = Files.readString(Path.of(filePath));
                ObjectMapper om = new ObjectMapper();
                testCaseCollection = om.readValue(dataString, TestCaseCollection.class);
            } catch (NoSuchFileException ignore) {
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Gets test case.
     *
     * @param digest the digest
     * @param name   the name
     * @return the test case
     */
    public TestCaseEntity getTestCase(Integer digest, String name) {
        StackUtil.log_function(print_functions);

        // In the odd chance that the TzatzikiPlugin has not been initialized we
        // get no name, and need to end quickly.
        if (name == null) return null;

        try {
            return testCaseCollection.getTestCase(digest, name);
        }
        catch (NoSuchElementException ignore) {
            if (dataBase()) {
                TestCaseEntity ret = getTestCaseFromDatabase(digest, name);
                testCaseCollection.getTestCases().add(ret);
                return ret;
            } else {
                TestCaseEntity ret = new TestCaseEntity(digest, name);
                testCaseCollection.getTestCases().add(ret);
                return ret;
            }
        }
    }

    /**
     * Persist state to storage.
     */
    public void persist() {
        StackUtil.log_function(print_functions);

        String filePath = properties.getString("tzatziki.file.path");

        if (dataBase()) {
            for (TestCaseEntity testCase : testCaseCollection.getTestCases()) {
                merge(testCase);
            }
        } else if (!filePath.isEmpty()) {
            try {
                ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
                String collectionString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(testCaseCollection);
                Files.writeString(Path.of(filePath), collectionString);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private TestCaseEntity getTestCaseFromDatabase(Integer digest, String name) {
        StackUtil.log_function(print_functions);

        // Prepare default return value
        TestCaseEntity ret = new TestCaseEntity(digest, name);

        // Open a new session
        Session session = sessionFactory.openSession();
        try {
            // Do things in a transaction
            session.beginTransaction();

            // Create a query for all test cases with matching name
            HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<TestCaseEntity> cr = cb.createQuery(TestCaseEntity.class);
            Root<TestCaseEntity> r = cr.from(TestCaseEntity.class);
            CriteriaQuery<TestCaseEntity> cq = cr.select(r).where(cb.equal(r.get("name"), name));
            Query<TestCaseEntity> q = session.createQuery(cq);

            // Remove all test cases with the wrong digest to avoid filling up the database
            for (TestCaseEntity entity : q.list()) {
                if (!Objects.equals(entity.getDigest(), digest)) {
                    session.remove(entity);
                }
            }

            // Examine what we have left, and possibly remove it if we should reset
            List<TestCaseEntity> list = q.list();
            if (!list.isEmpty()) {
                if (tzatziki_reset) session.remove(list.get(0));
                else ret = list.get(0);
            }

            // We save anything that we changed
            session.getTransaction().commit();
        }
        catch (Exception e) {
            // We roll back if unsuccessful
            if (session.getTransaction()!=null) session.getTransaction().rollback();
            e.printStackTrace();
        }
        finally {
            // We always close the session
            session.close();
        }

        // We return database entity if possible but at least a new entity
        return ret;
    }

    private void merge(TestCaseEntity testCase) {
        StackUtil.log_function(print_functions);

        // Open a new session
        Session session = sessionFactory.openSession();
        try {
            // Do things in a transaction
            session.beginTransaction();

            // Simply do a merge which will either update or create entries
            session.merge(testCase);

            // Finally, commit the transaction and all will be saved
            session.getTransaction().commit();
        }
        catch (Exception e) {
            // We roll back if unsuccessful
            if (session.getTransaction()!=null) session.getTransaction().rollback();
            e.printStackTrace();
        }
        finally {
            // We always close the session
            session.close();
        }
    }

    private boolean dataBase() {
        return (sessionFactory != null);
    }
}

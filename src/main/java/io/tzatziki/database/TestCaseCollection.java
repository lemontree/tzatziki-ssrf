package io.tzatziki.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.tzatziki.properties.TzatzikiProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.*;

/**
 * Test case collection - for storing and retrieving test cases.
 */
@Getter
@Setter
@Accessors(chain = true)
public class TestCaseCollection {
    private final List<TestCaseEntity> testCases;

    @JsonIgnore
    private final TzatzikiProperties properties = new TzatzikiProperties();
    @JsonIgnore
    private final Set<String> testCasesRead;
    @JsonIgnore
    private final boolean tzatziki_reset = properties.getBoolean("tzatziki.reset");

    /**
     * Instantiates a new Test case collection.
     */
    public TestCaseCollection() {
        testCases = new ArrayList<>();
        testCasesRead = new HashSet<>();
    }

    /**
     * Gets test case from collection if found.
     *
     * @param digest the digest
     * @param name   the name
     * @return the test case
     * @throws NoSuchElementException the no such element exception
     */
    public TestCaseEntity getTestCase(Integer digest, String name) throws NoSuchElementException {
        boolean firstReference = !(testCasesRead.contains(name));
        for (TestCaseEntity testCase : testCases) {
            if (name.equals(testCase.getName())) {
                if (digest.equals(testCase.getDigest())) {
                    if (firstReference && tzatziki_reset) {
                        testCases.remove(testCase);
                        break;
                    } else {
                        return testCase;
                    }
                } else {
                    testCases.remove(testCase);
                    break;
                }
            }
        }
        if (firstReference) testCasesRead.add(name);
        throw new NoSuchElementException("No matching entity found.");
    }
}

package io.tzatziki.database;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "test_case")
public class TestCaseEntity {
    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue
    @Column(nullable = false)
    private Long id;

    @Column(name = "digest", nullable = false)
    private Integer digest;

    @Column(name = "name", nullable = false)
    private String name;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "test_step", joinColumns = { @JoinColumn(name = "test_case", referencedColumnName = "id")})
    @MapKeyColumn(name = "step_line")
    @Column(name = "step_state", length = 10)
    private Map<Integer, String> stepState;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "test_store", joinColumns = { @JoinColumn(name = "test_case", referencedColumnName = "id")})
    @MapKeyColumn(name = "store_class")
    @Column(name = "store", length = 10000)
    private Map<String, String> storage;

    public TestCaseEntity() {}

    public TestCaseEntity(Integer digest, String name) {
        this.digest = digest;
        this.name = name;
        stepState = new HashMap<>();
        storage = new HashMap<>();
    }

    @Override
    public String toString() {
        return "TestCaseEntity{" +
                "id=" + id +
                ", digest=" + digest +
                ", name='" + name + '\'' +
                ", stepState=" + stepState +
                ", storage=" + storage +
                '}';
    }
}
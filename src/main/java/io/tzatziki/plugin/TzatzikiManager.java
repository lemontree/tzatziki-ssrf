package io.tzatziki.plugin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.tzatziki.database.TestCaseEntity;
import io.tzatziki.database.TestCaseManager;
import io.tzatziki.properties.TzatzikiProperties;
import io.tzatziki.utility.StackUtil;

/**
 * Tzatziki manager - keeping track of stored state and other things that are necessary for the successful
 * organization of the Tzatziki solution.
 */
public class TzatzikiManager {
    private final TzatzikiProperties properties = new TzatzikiProperties();
    private final TestCaseManager testCaseManager = new TestCaseManager();
    private final boolean print_functions = properties.getBoolean("print.manager.functions");

    /**
     * Instantiates a new Tzatziki manager.
     */
    public TzatzikiManager() {
        StackUtil.log_function(print_functions);
    }

    /**
     * Persist state.
     */
    public void persist() {
        StackUtil.log_function(print_functions);

        testCaseManager.persist();
    }

    /**
     * Gets test step state.
     *
     * @param testCaseHash the test case hash
     * @param testCaseName the test case name
     * @param testStepLine the test step line
     * @return the test step state
     */
    public String getTestStepState(int testCaseHash, String testCaseName, int testStepLine) {
        StackUtil.log_function(print_functions);

        TestCaseEntity testCase = testCaseManager.getTestCase(testCaseHash, testCaseName);
        String stepState = null;
        try {
            stepState = testCase.getStepState().get(testStepLine);
        } catch (Exception ignore) {}
        return stepState == null ? "UNKNOWN" : stepState;
    }

    /**
     * Sets test step state.
     *
     * @param testCaseHash  the test case hash
     * @param testCaseName  the test case name
     * @param testStepLine  the test step line
     * @param testStepState the test step state
     */
    public void setTestStepState(int testCaseHash, String testCaseName, int testStepLine, String testStepState) {
        StackUtil.log_function(print_functions);

        TestCaseEntity testCase = testCaseManager.getTestCase(testCaseHash, testCaseName);
        if (testCase.getStepState().containsKey(testStepLine))
            testCase.getStepState().replace(testStepLine, testStepState);
        else
            testCase.getStepState().put(testStepLine, testStepState);
    }

    /**
     * Gets storage.
     *
     * @param testCaseHash     the test case hash
     * @param testCaseName     the test case name
     * @param storageClassName the storage class name
     * @return the storage, or null if not found.
     */
    public Object getStorage(int testCaseHash, String testCaseName, String storageClassName) {
        StackUtil.log_function(print_functions);

        TestCaseEntity testCase = testCaseManager.getTestCase(testCaseHash, testCaseName);

        // Try to get storage from storage, with a complete match needed.
        Object ret = null;
        try {
            Class<?> storageClass = TzatzikiManager.class.getClassLoader().loadClass(storageClassName);
            String dataString = testCase.getStorage().get(storageClassName);
            ObjectMapper om = new ObjectMapper();
            ret = om.readValue(dataString, storageClass);
        } catch (JsonProcessingException | ClassNotFoundException | NullPointerException | IllegalArgumentException ignore) {
        }

        // Return whatever we have found.
        return ret;
    }

    /**
     * Sets storage.
     *
     * @param testCaseHash     the test case hash
     * @param testCaseName     the test case name
     * @param storageClassName the storage class name
     * @param storageObject    the storage object
     */
    public void setStorage(int testCaseHash, String testCaseName, String storageClassName, Object storageObject) {
        StackUtil.log_function(print_functions);

        try {
            TestCaseEntity testCase = testCaseManager.getTestCase(testCaseHash, testCaseName);

            ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
            String storageString = mapper.writeValueAsString(storageObject);
            if (testCase.getStorage().containsKey(storageClassName))
                testCase.getStorage().replace(storageClassName, storageString);
            else
                testCase.getStorage().put(storageClassName, storageString);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}

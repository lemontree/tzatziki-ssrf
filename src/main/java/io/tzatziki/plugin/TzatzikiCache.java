package io.tzatziki.plugin;

import io.cucumber.plugin.event.PickleStepTestStep;
import io.cucumber.plugin.event.TestCase;
import io.tzatziki.TzatzikiInitializer;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Tzatziki Cache - Singleton data holder used by the Plugin and end user Tzatziki contained object.
 */
@Getter
@Setter
@Accessors(chain = true)
public class TzatzikiCache {
    private TestCase testCase;
    private List<PickleStepTestStep> testSteps;
    private int testCaseDigest;
    private String testCaseName;
    private int testCaseLine;
    private String currentStep;
    private int testStepLine;
    private TzatzikiManager manager;
    private Map<Class<?>, TzatzikiInitializer> initializers;
    private Map<Class<?>, Boolean> initialized;
    private Map<Class<?>, Object> storages;
    private boolean lastPersistedStorageFailed = false;

    private TzatzikiCache() {
        manager = new TzatzikiManager();
        initializers = new HashMap<>();
        initialized = new HashMap<>();
        storages = new HashMap<>();
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static TzatzikiCache getInstance() {
        return CacheHolder.INSTANCE;
    }

    /**
     * Register initializer getting callbacks to initialize storage before execution.
     *
     * @param storageClass the storage class
     * @param initializer  the initializer
     */
    public void registerInitializer(Class<?> storageClass, TzatzikiInitializer initializer) {
        initializers.remove(storageClass);
        initializers.put(storageClass, initializer);
    }

    /**
     * Gets initializers.
     *
     * @return the initializers
     */
    public Map<Class<?>, TzatzikiInitializer> getInitializers() {
        return initializers;
    }

    /**
     * Gets initialized state of a given initializer.
     *
     * @param storage the storage
     * @return the initialized
     */
    public boolean getInitialized(Class<?> storage) {
        if (!initialized.containsKey(storage)) {
            initialized.put(storage, false);
        }
        return initialized.get(storage);
    }

    /**
     * Sets initialized state for a given initializer.
     *
     * @param storage the storage
     * @param init    the init
     */
    public void setInitialized(Class<?> storage, boolean init) {
        if (!initialized.containsKey(storage)) {
            initialized.put(storage, init);
        } else {
            initialized.replace(storage, init);
        }
    }

    /**
     * Sets live storage for future serialization.
     *
     * @param liveClass the live class
     * @param live      the live object
     */
    public void setLiveStorage(Class<?> liveClass, Object live) {
        if (!storages.containsKey(liveClass)) {
            storages.put(liveClass, live);
        } else {
            storages.replace(liveClass, live);
        }
    }

    /**
     * Gets storage from serialized state for given class.
     *
     * @param storageClass the storage class
     * @return the storage
     */
    public Object getPersistedStorage(Class<?> storageClass) {
        Object instance = manager.getStorage(testCaseDigest, testCaseName, storageClass.getName());
        lastPersistedStorageFailed = (instance == null);
        return instance;
    }

    /**
     * Gets step state.
     *
     * @return the step state
     */
    public String getStepState() {
        if (lastPersistedStorageFailed) return "UNKNOWN";
        return manager.getTestStepState(testCaseDigest, testCaseName, testStepLine);
    }

    /**
     * Sets step state.
     *
     * @param state the state
     */
    public void setStepState(String state) {
        manager.setTestStepState(testCaseDigest, testCaseName, testStepLine, state);
    }

    /**
     * Flush the state of the cache.
     */
    public void flush() {
        manager.persist();
    }

    /**
     * Update storages used in current test case.
     */
    public void updateStorage() {
        for (Class<?> storageClass : storages.keySet()) {
            manager.setStorage(testCaseDigest, testCaseName, storageClass.getName(), storages.get(storageClass));
        }
    }

    private static class CacheHolder {
        private static final TzatzikiCache INSTANCE = new TzatzikiCache();
    }
}

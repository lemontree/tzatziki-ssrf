package io.tzatziki.plugin;

import io.cucumber.plugin.ConcurrentEventListener;
import io.cucumber.plugin.event.*;
import io.tzatziki.properties.TzatzikiProperties;
import io.tzatziki.utility.StackUtil;

import java.util.ArrayList;

import static io.tzatziki.utility.AnsiColor.CYAN;
import static io.tzatziki.utility.AnsiColor.RESET;

/**
 * Tzatziki plugin - The main transport vehicle of the Tzatziki solution of Start - Stop - Restart.
 *
 * The plugin will provide the step definition implementation with the necessary information for
 * deciding on skipping the execution of a step and report "Skipped but OK" and a status of PASSED.
 */
public class TzatzikiPlugin implements ConcurrentEventListener {

    private final TzatzikiCache tzatziki = TzatzikiCache.getInstance();
    private final TzatzikiProperties properties = new TzatzikiProperties();
    private final boolean print_functions = properties.getBoolean("print.plugin.functions");
    private final boolean print_testcase  = properties.getBoolean("print.plugin.testcase");
    private final boolean print_teststep  = properties.getBoolean("print.plugin.teststep");

    @Override
    public void setEventPublisher(EventPublisher eventPublisher) {
        eventPublisher.registerHandlerFor(TestRunStarted.class, this::testRunStarted);
        eventPublisher.registerHandlerFor(TestCaseStarted.class, this::testCaseStarted);
        eventPublisher.registerHandlerFor(TestStepStarted.class, this::testStepStarted);
        eventPublisher.registerHandlerFor(TestStepFinished.class, this::testStepFinished);
        eventPublisher.registerHandlerFor(TestCaseFinished.class, this::testCaseFinished);
        eventPublisher.registerHandlerFor(TestRunFinished.class, this::testRunFinished);
    }

    @SuppressWarnings("unused")
    private void testRunStarted(TestRunStarted testRunStarted) {
        StackUtil.log_function(print_functions);
    }

    @SuppressWarnings("unused")
    private void testRunFinished(TestRunFinished testRunFinished) {
        StackUtil.log_function(print_functions);
        tzatziki.flush();
    }

    private void testCaseStarted(TestCaseStarted testCaseStarted) {
        StackUtil.log_function(print_functions);

        // Save all information
        tzatziki.setTestCase(testCaseStarted.getTestCase());
        tzatziki.setTestSteps(new ArrayList<>());
        for (TestStep testStep : tzatziki.getTestCase().getTestSteps())
            if (testStep instanceof PickleStepTestStep)
                tzatziki.getTestSteps().add((PickleStepTestStep) testStep);
        tzatziki.setTestCaseLine(testCaseStarted.getTestCase().getLocation().getLine());

        // Calculate hash of the testcase, including the layout of the feature file.
        StringBuilder acc = new StringBuilder(tzatziki.getTestCase().getName());
        for (PickleStepTestStep testStep : tzatziki.getTestSteps())
            acc.append("[").append(testStep.getStep().getLine() - tzatziki.getTestCaseLine()).append("]")
                    .append(testStep.getStep().getText());
        tzatziki.setTestCaseDigest(acc.toString().hashCode());
        tzatziki.setTestCaseName(tzatziki.getTestCase().getName());

        // Print test case
        if (print_testcase) {
            System.out.println(CYAN + "-".repeat(100));
            System.out.println(" Test Case Started - \"" + tzatziki.getTestCase().getName() + "\"");
            System.out.println("-".repeat(100));
            for (PickleStepTestStep testStep : tzatziki.getTestSteps())
                System.out.println("[" + (testStep.getStep().getLine() - tzatziki.getTestCaseLine()) + "] " +
                        testStep.getStep().getText());
            System.out.print(RESET);
        }
    }

    private void testCaseFinished(TestCaseFinished testCaseFinished) {
        StackUtil.log_function(print_functions);

        // Just print test case
        if (print_testcase) {
            System.out.println(CYAN + "-".repeat(100));
            System.out.println(" Test Case Finished");
            System.out.println("-".repeat(100));
            System.out.println("status = " + testCaseFinished.getResult().getStatus().toString());
            System.out.print(RESET);
        }
    }

    private void testStepStarted(TestStepStarted testStepStarted) {
        StackUtil.log_function(print_functions);
        if (testStepStarted.getTestStep() instanceof PickleStepTestStep) {
            final PickleStepTestStep testStep = (PickleStepTestStep) testStepStarted.getTestStep();
            tzatziki.setCurrentStep(testStep.getStep().getText());
            tzatziki.setTestStepLine(testStep.getStep().getLine() - tzatziki.getTestCaseLine());

            if (print_teststep) {
                System.out.println(CYAN + "-".repeat(100));
                System.out.println(" Test Step Started - \"" + testStep.getStep().getText() + "\"");
                System.out.println("-".repeat(100));
                System.out.println("line    = " + tzatziki.getTestStepLine());
                System.out.println("text    = " + testStep.getStep().getText());
                System.out.println("keyword = " + testStep.getStep().getKeyword());
                System.out.println("pattern = " + testStep.getPattern());
                System.out.println("state   = " + tzatziki.getStepState());
                System.out.print(RESET);
            }
        }
    }

    private void testStepFinished(TestStepFinished testStepFinished) {
        StackUtil.log_function(print_functions);

        tzatziki.setStepState(testStepFinished.getResult().getStatus().toString());

        if (testStepFinished.getResult().getStatus().is(Status.PASSED))
            tzatziki.updateStorage();

        if (print_teststep) {
            System.out.println(CYAN + "-".repeat(100));
            System.out.println(" Test Step Finished");
            System.out.println("-".repeat(100));
            System.out.println("status = " + tzatziki.getStepState());
            System.out.print(RESET);
        }
    }
}

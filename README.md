# tzatziki-core
A Start-Stop-Resume framework for E2E Cucumber tests based on PicoContainer and Hibernate.

## History
**Lemontree Enterprise Solutions AB** is a Swedish consulting company very much dedicated to quality 
assurance. Since 2010 Lemontree has developed test frameworks capable of start-stop-resume execution 
based on third party tools, databases and proprietary code. With the diverse test industry going in 
all directions towards both non-code and all-code solutions, it was inevitable that our experience 
would end up in a tool for framework development based on industry BDD standards.

## Applicability
Under certain circumstances it is possible to separate the steps of a Cucumber Scenario into atomic 
operations with little or no interdependencies but for data. The poster-child example would be an 
order flow where operations are performed in their entirety within a step, each step only depending
on the data output from the previous step and not by the internal state of the application under test.
One step creates an order and outputs an order number, next step refines the order or performs test
on the order, etc. With the inter-step data stored in a storage class instantiated by PicoContainer, 
a step does not have to know if the previous step was executed or if the data was simply entered in 
storage. Under these circumstances, storage can be persisted between test executions and steps can 
be skipped to save time or avoid wasting precious test data.

## Objective
The objective of tztziki-core is to combine the use of PicoContainer and Hibernate to make a base 
for an easy to handle, start-stop-resume framework for cucumber based tests. Tzatziki will, based on 
the features executed, enable resuming execution of previously executed tests while still leaving all 
the power to the test step developer.

## Implementation
A named scenario, together with its steps and explicit data, produces a signature hash value. Scenario 
with name and hash value, together with its step execution results and all its pico-container storages  
are persisted either in a local file or a database of choice. Next execution with matching scenario name 
and hash value will be given the opportunity to restore its persisted storages and query step state 
for possible skips. 

- **Storage restoration** is performed by the step implementer by registering a callback function receiving 
a storage copy to restore data from. This gives the user of tzatziki-core the responsibility and full 
control of the restoration process. 

- **Step skipping** is done by asking tzatziki if the current step is possible to skip. If possible then a 
skip is performed by the step by simply returning early. This gives the user of tzatziki-core full 
control of skipping a step or re-running it despite the stored possibility of a skip. 

- **Resetting** execution can be performed by configuration or input to maven, or by deleting the persisted 
data either in the database or in the local file. This gives the user of tzatziki-core control over when the 
resume mechanism should not apply.

### Scenarios
The unique name and set of steps, including data, are used as keys for determining if something has changed 
and the persisted information should be discarded. If the signature of the scenario has changed, the entire  
set of data will be ignored and execution will start from the beginning with no restoration of storage. 
Given that resume executions should be done with the same conditions as the original execution this comes 
natural. This does however give the user of tzatziki-core the opportunity to change the implementation of 
steps in the presence of bugs in the test code, and still resume execution. A feature that comes in handy 
in the test step development phase. 

##### --- Sample feature file ---
```gherkin
Feature: Sample feature in tzatziki-core documentation

  @mytest
  Scenario: Testing out the framework
    Given Order engine is initialized
    Then We should place an order
    And The order should contain 7 items
```

### Restoring Storage
In your step definition class, add an instance of the Tzatziki class, handled by PicoContainer constructor
dependency injection. In your step definition class constructor, add a call to register an initializer 
function for each storage class you want to handle restoring. 

##### -- Sample StepDefinition class with initialization sections --
```java
import io.tzatziki.Tzatziki;

public class StepDefintion { 
    private final Tzatziki tzatziki;
    private final StepStorage storage;
    
    public StepDefintion(Tzatziki tzatziki, StepStorage storage) {
        this.tzatziki = tzatziki;
        this.storage = storage;
        
        // We register ourselves as initializer of the storage since we know it
        tzatziki.registerInitializer(StepStorage.class, this::initStepStorage);
    }

    private void initStepStorage(Object live, Object store) {
        // We can safely cast to correct storage type since we registered the
        // init function for this class only. 
        StepStorage ls = (StepStorage) live;
        StepStorage ss = (StepStorage) store;
  
        // We use our knowledge to copy the state from store to live object.
        ls.setOrderNumber(ss.getOrderNumber());
        ls.setFoo(ss.getFoo());
        ls.setBar(ss.getBar());
    }   
    
    ...
}
```

### Skipping Step Execution
If a step has been previously recorded as successful you can skip its execution in a resumed run. In the 
step definition class a call to the _skippableStep_ method of the Tzatziki class will tell if a skip is 
possible.

##### -- Sample StepDefinition class with step functions --
```java
public class StepDefintion {
    ...

    @Then("We should place an order")
    public void weShouldPlaceAnOrder() {
        if (tzatziki.skippableStep()) return;
        
        // Here we place an order. Something that takes a long time and consumes test data.
        OrderMotor orderMotor = new OrderMotor();
        storage.setOrderNumber(orderMotor.placeOrder());
    }
    
    @And("The order should contain {int} items")
    public void theOrderShouldContainItems(int numItems) {
        if (tzatziki.skippableStep()) return;
      
        // We verify order content using either fresh or stored order number. 
        OrderVerifier orderVerifier = new OrderVerifier(storage.getOrderNumber());
        assertThat(orderVerifier.getNumItems()).equals(numItems);
    }
    
    ...
}
```

### Persisted Storage
Storage class data is persisted to file or database in plain text format in version 1.0. This means that 
sensitive data should absolutely be excluded from being persisted. As we use Json to serialize objects, 
storage classes may contain sensitive data if it is marked as transient, as transient data is not persisted. 
This also means that transient data will not be available for restoration during execution. 

##### -- Sample StepStorage class with sensitive data --

```java
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StepStorage {
    private String orderNumber;
    private Integer foo;
    private Integer bar;
    private transient String socialSecurityNumber;
    private transient LoginCredentials loginCredentials;
    
    ...
}
```

## Configuration
A number of values can be configured to alter the behavior of tzatziki-core.  

### tzatziki.properties
Place a file named tzatziki.properties in your _resources_ folder. Database entries use the same syntax as 
that of Hibernate, but will not interfere with any Hibernate configuration file present in the application. 

* **tzatziki.file.path** = 
    - Set to a path (relative to the project) to have state serialized to a file.
* **tzatziki.db.url** = 
    - Set to the URL of a database datasource. Will override file usage.
* **tzatziki.db.driver** =
    - Set to a database driver (needed if url is specified)
* **tzatziki.db.username** =
    - Set to a database username (needed if url is specified)
* **tzatziki.db.password** =
    - Set to a database password (needed if url is specified)
* **tzatziki.reset** = 
    - Set to true to change default mode to discard persisted data.

##### -- Sample tzatziki.properties with database entries --
```properties
# Set to a JDBC driver to have state serialized to database (prioritized over file)
# To avoid setting password in checked in file, pass it as a parameter to maven and
# store it in a CI/CD secret.

tzatziki.db.url      = jdbc:postgresql://localhost/tzatziki
tzatziki.db.driver   = org.postgresql.Driver
tzatziki.db.username = databaseuser

# Set to a path (relative to the project) to have state serialized to a file.
# If database entries are set, the file path will be ignored.

tzatziki.file.path = target/tzatziki.json
```

### Command Line
All configuration values can be overridden by setting the corresponding value in the call to **maven** via 
-Dkey=value syntax. This is especially useful for **password** and **reset** values.

##### -- Sample command lines --
```shell
-- Run tagged test via the runner CucumberTest setting the database password --

mvn test -Dtest=CucumberTest -Dcucumber.filter.tags="@mytest" -Dtzatziki.db.password=secretpassword

-- Run tagged test resetting execution from start of test

mvn test -Dtest=CucumberTest -Dcucumber.filter.tags="@mytest" -Dtzatziki.db.password=secretpassword -Dtzatziki.reset=true
```
